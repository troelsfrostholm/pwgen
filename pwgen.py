#!/usr/local/bin/python3

import sys
import string
import re
from random import randint, choice

def triad():
  return choice(uco)+choice(wo)+choice(lco)

def num():
  return str(randint(0,9))

def usage():
  print("Usage: pwgen.py password-length")
  print("password-length must be an integer")

if(len(sys.argv)<2):
  usage()
  exit()

if(re.match(r"^[0-9]*$", sys.argv[1])==None):
  usage()
  exit()

N = int(sys.argv[1])
lc = string.ascii_lowercase
uc = string.ascii_uppercase

wo = "aeiou"
lco = "bcdfghjklmnpqrstvwxyz"
uco = "BCDFGHJKLMNPQRSTVWXYZ"

pwd = "".join([num()+triad() for i in range(int(N/4)+1)])[0:N]
print(pwd)
