# README #

pwgen is a minimal phonetic password generator. 

## Installation and running
pwgen depends on python3 only. Get it at https://www.python.org. 
You also need git to fetch the code. Get it at http://git-scm.com. 

In a terminal (on either mac, linux or windows) write:

    > git clone https://bitbucket.org/troelsfrostholm/pwgen.git
    > cd pwgen
    > python3 pwgen.py 8
    5Yix0Kuc

In the example, 8 is the length of the password.